# API for The Holy Qur'an and Persian Translation

<div style=text-align:center;>

![Bismillah](./bismillah.jpg)

</div>

Qur'an or *The Holy Qur'an*, is the central religious text of Islam. Qur'an is brought to prophet Muhammad (Peace be Upon Him) by Allh more than one thousand years ago, and It was in the language of Arabic. As Allah Himself says : 

```
Indeed, We have sent it down as an Arabic Qur'an that you might understand.

Quran(12:2)
``` 

After that, the religion of Islam developed to some other points of the world. North of Africa, West of Europe, and a big part of Asia, including south west and even south east Asia. Muslims in Asia are living in Turkey, Iran, Iraq, Jordan, Saudi Arabia, Yemen, Bahrain, Indonesia, etc. 

So, after this great development, most of people started translating Qur'an to their native languages. As an Iranian, I learned Arabic in school to understand Qur'an better, but I still prefer to read the persian translation, which obviously helps me understand my religion. 

## Why I started this project? 

I usually use [quran.com](http://quran.com) as a reference for Qur'an and different translations. I was looking for a simple and lightweight API, which only includes Arabic and Persian text of Qur'an. So, I asked on twitter if there's any, one of my friends just sent me a database, and that was exactly what I wanted. Unfortunately, no one has developed any API's using that useful database, so I decided to make one myself. 

## API Reference 

The API is currently under development, but there will be a reference, as soon as possible. 