FROM ruby:2.4-onbuild 

RUN mkdir -pv /usr/src/app
RUN echo "Folders made successfully" 

ADD . /usr/src/app 
WORKDIR /usr/src/app 

CMD ["ruby", "/usr/src/app/app.rb"]