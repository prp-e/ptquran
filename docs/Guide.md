# Persian Qur'an API guide 

In the name of God, the compassionate, the merciful. 

Finally, I could finish this product and now here is the result you can use. Following lines will help you have access to the API. 

## API address 

API is currently hosted on `fandogh` a great Iranian tool to just deploy your projects. The current address is : 

```
ptquran-haghiri75.fandogh.cloud 
``` 

And every request you send should be sent to that address. 

### More deployments 

If you can deploy this on another servers, please do that and send a merge request. 

## Requests Guide 

### List of Surah's by name 

```
GET /suraNames 
``` 

### Name of a certain Surah by ID 

```
GET /surah/:id 
``` 

**NOTE** : Valid ID's are 1 to 114 

### Nozul place of the Surah 

```
GET /surah/:id/nozul 
``` 

### Arabic text of a Surah (whole verses) 

```
GET /ar/surah/:sid 
``` 

### Arabic text of a Surah (a certain verse)

```
GET /ar/surah/:sid/ayah/:aid
``` 

### Persian text of a Surah (whole verses)

```
GET /fa/surah/:sid
```

### Persian text of a Surah (a certain verse)

```
GET /fa/surah/:sid/ayah/:aid 
``` 
