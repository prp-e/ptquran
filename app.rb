require 'sinatra'
require 'sinatra/cross_origin'
require 'sqlite3'
require 'json'

# Primary Configuration 

set :bind, '0.0.0.0' 

configure do 
    enable :cross_origin 
end  

before do 
    response.headers['Access-Control-Allow-Origin'] = '*'
end 

options "*" do
    response.headers["Allow"] = "GET, PUT, POST, DELETE, OPTIONS"
    response.headers["Access-Control-Allow-Headers"] = "Authorization, Content-Type, Accept, X-User-Email, X-Auth-Token"
    response.headers["Access-Control-Allow-Origin"] = "*"
    200
end

# Database Setup 

db = SQLite3::Database.open('Quran.db') 

# Routes 

get '/suraNames' do 
    
    content_type :json 
    suranames = db.execute("SELECT * FROM SuraNames")
    suranames = suranames.to_json  

end

get '/surah/:id' do 
    
    content_type :json 
    surah = db.execute("SELECT SuraName FROM SuraNames WHERE SuraID=#{params[:id]}")
    surah = surah.to_json 

end

get '/surah/:id/nozul' do 

    content_type :json 
    nozul = db.execute("SELECT Nozul FROM SuraNames WHERE SuraID=#{params[:id]}")
    nozul = nozul.to_json 

end

get '/ar/surah/:sid' do 

    content_type :json 
    surah = db.execute("SELECT AyahText FROM Quran WHERE SuraID=#{params[:sid]}")
    surah = surah.to_json 

end 

get '/ar/surah/:sid/ayah/:aid' do 

    content_type :json 
    ayah = db.execute("SELECT AyahText FROM Quran WHERE SuraID=#{params[:sid]} AND VerseID=#{params[:aid]}")
    ayah = ayah.to_json 

end 

get '/fa/surah/:sid' do 
    
    content_type :json 
    surah = db.execute("SELECT AyahText From PTQuran WHERE SuraID=#{params[:sid]}")
    surah = surah.to_json 
end 

get '/fa/surah/:sid/ayah/:aid' do 

    content_type :json 
    surah = db.execute("SELECT AyahText FROM PTQuran WHERE SuraID=#{params[:sid]} AND VerseID=#{params[:aid]}")
    surah = surah.to_json 

end 

get '/json_api/surah/:sid' do 
    
    content_type :json 

    surah_name = db.execute("SELECT SuraName FROM SuraNames WHERE SuraID=#{params[:sid]}")
    surah_nozul = db.execute("SELECT Nozul FROM SuraNames WHERE SuraID=#{params[:sid]}") 
    surah_verses = db.execute("SELECT Verses FROM SuraNames WHERE SuraID=#{params[:sid]}")

    surah_details = {"Name": surah_name[0][0], "Nozul": surah_nozul[0][0], "Verses": surah_verses[0][0]}
    surah_details = surah_details.to_json 

end

get '/json_api/surah/complete/:sid' do 

    content_type :json 
    surah_name = db.execute("SELECT SuraName FROM SuraNames WHERE SuraID=#{params[:sid]}")
    surah_nozul = db.execute("SELECT Nozul FROM SuraNames WHERE SuraID=#{params[:sid]}") 
    surah_verses = db.execute("SELECT Verses FROM SuraNames WHERE SuraID=#{params[:sid]}")

    surah_details = {"Name": surah_name[0][0], "Nozul": surah_nozul[0][0], "Verses": surah_verses[0][0]}

    surah_ar = db.execute("SELECT AyahText FROM Quran WHERE SuraID=#{params[:sid]}")
    surah_fa = db.execute("SELECT AyahText FROM PTQuran WHERE SuraID=#{params[:sid]}")

    surah_ar_verses = [] 
    surah_fa_verses = [] 

    for ayah in surah_ar do 
        surah_ar_verses.push(ayah[0])
    end 

    for ayah in surah_fa do 
        surah_fa_verses.push(ayah[0])
    end 

    surah_text = {"details": surah_details, "arabic_text": surah_ar_verses, "persian_text": surah_fa_verses}
    surah_text = surah_text.to_json

end

get '/json_api/surah/:sid/ayah/:aid' do 

    content_type :json

    surah_name = db.execute("SELECT SuraName FROM SuraNames WHERE SuraID=#{params[:sid]}")
    surah_nozul = db.execute("SELECT Nozul FROM SuraNames WHERE SuraID=#{params[:sid]}") 
   
    ayah_text = db.execute("SELECT AyahText FROM Quran Where SuraID=#{params[:sid]} AND VerseID=#{params[:aid]}")
    ayah_translation = db.execute("SELECT AyahText FROM PTQuran Where SuraID=#{params[:sid]} AND VerseID=#{params[:aid]}")

    surah_name = db.execute("SELECT SuraName FROM SuraNames WHERE SuraID=#{params[:sid]}")
    surah_nozul = db.execute("SELECT Nozul FROM SuraNames WHERE SuraID=#{params[:sid]}")

    surah_details = {"Nozul": surah_nozul[0][0], "Name": surah_name[0][0]}
    data = {"ayah_ar": ayah_text[0][0], "ayah_fa": ayah_translation[0][0], "Details": surah_details}
    data = data.to_json 
end